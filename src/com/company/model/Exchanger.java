package com.company.model;

import java.util.Locale;

public class Exchanger {
    private String name;
    private float course;
    private int limit;

    public Exchanger(String name, float course, int limit) {
        this.name = name;
        this.course = course;
        this.limit = limit;
    }

    public String convert(int count) {
        if (count < limit) {
            return String.format(Locale.US, "%.2f", count * this.course * 0.95f);
        }
        return null;
    }

    public String getName() {
        return name;
    }
}
