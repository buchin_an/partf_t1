package com.company.model;

import java.util.Locale;

public class Bank {
    private String name;
    private float course;
    private int limit = 150000;

    public Bank(String name, float course) {
        this.name = name;
        this.course = course;
    }

    public String convert(int count) {
        if (count * this.course * 0.95f < this.limit) {
            return String.format(Locale.US, "%.2f", count * this.course * 0.95f);
        }
        return null;
    }

    public String getName() {
        return name;
    }
}
