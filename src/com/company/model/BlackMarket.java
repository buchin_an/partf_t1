package com.company.model;

import java.util.Locale;

public class BlackMarket {
    private String name;
    private float course;

    public BlackMarket(String name, float course) {
        this.name = name;
        this.course = course;
    }

    public String convert(int count) {
        return String.format(Locale.US, "%.2f", count * this.course);
    }
}
