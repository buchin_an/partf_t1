package com.company;

import com.company.model.Bank;
import com.company.model.BlackMarket;
import com.company.model.Exchanger;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Bank[] banks = new Bank[3];
        Exchanger[] exchangers = new Exchanger[3];

        banks[0] = new Bank("private", 26.5f);
        banks[1] = new Bank("public", 25.53f);
        banks[2] = new Bank("protected", 23.5f);

        exchangers[0] = new Exchanger("sharaga", 22.6f, 1000);
        exchangers[1] = new Exchanger("shar", 27.6f, 2000);
        exchangers[2] = new Exchanger("sh", 28.6f, 3000);

        BlackMarket blackMarket = new BlackMarket("black", 23.3f);

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter amount");
        int amount = Integer.valueOf(scanner.nextLine());

        for (int i = 0; i < 3; i++) {
            if (banks[i].convert(amount) != null) {
                System.out.println("Bank " + banks[i].getName());
                System.out.println(banks[i].convert(amount));
            }
            if (exchangers[i].convert(amount) != null) {
                System.out.println("Exchanger " + exchangers[i].getName());
                System.out.println(exchangers[i].convert(amount));
            }

        }
        System.out.println("Black market " + blackMarket.convert(amount));
    }

}
